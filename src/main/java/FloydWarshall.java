import ancillary.Edge;

import java.util.List;

import static java.lang.Integer.*;

public class FloydWarshall {
    private Integer[][] shortest_paths;
    private Integer[][] predecessors;

    private Integer number_of_vertices;
    private Integer number_of_edges;

    private String path;

    private FloydWarshall(List<Edge> edges, int graph_order) {
        this.number_of_vertices = graph_order;
        this.number_of_edges = edges.size();

        this.shortest_paths = default_shortest_paths(MAX_VALUE);
        this.predecessors = default_predecessors(-1);

        mark_direct_paths(edges);
    }

    public static FloydWarshall forGraph(List<Edge> edges, int number_of_vertices) {
        return new FloydWarshall(edges, number_of_vertices);
    }

    public Boolean calculate() {
        for (int k=0; k<number_of_vertices; k++) {
            for (int i=0; i<number_of_vertices; i++) {
                for (int j=0; j<number_of_vertices; j++) {
                    if ( (shortest_paths[i][k] != MAX_VALUE) && (shortest_paths[k][j] != MAX_VALUE) ) {
                        if ( shortest_paths[i][j] > shortest_paths[i][k] + shortest_paths[k][j] ) {
                            shortest_paths[i][j] = shortest_paths[i][k] + shortest_paths[k][j];
                            predecessors[i][j] = predecessors[k][j];
                        }
                    }
                }
            }
        }
        for (int i = 0; i<number_of_vertices; i++) {
            if (shortest_paths[i][i] < 0 ) {
                return false;
            }
        }
        return true;
    }

    public String getPath(Integer start_vertex, Integer end_vertex) {
        this.path = String.format("Wage: %s | Path -> :", shortest_paths[start_vertex][end_vertex]);
        build_shortest_path_for(start_vertex, end_vertex);
        return this.path;
    }

    private void build_shortest_path_for(Integer start_vertex, Integer end_vertex) {
        if (start_vertex == end_vertex) {
            path += " ( " + start_vertex + " ) ";
        } else if ( predecessors[start_vertex][end_vertex] == -1 ) {
            path = "Path does not exist!";
        } else {
            build_shortest_path_for(start_vertex, predecessors[start_vertex][end_vertex]);
            path += " ( " + end_vertex + " ) ";
        }
    }

    private Integer[][] default_shortest_paths(Integer default_value) {
        Integer matrix[][] = new Integer[number_of_vertices][number_of_vertices];
        for (int i=0; i<number_of_vertices; i++) {
            for (int j=0; j<number_of_vertices; j++) {
                matrix[i][j] = i == j ? 0 : default_value;
            }
        }
        return matrix;
    }

    private Integer[][] default_predecessors(Integer default_value) {
        Integer matrix[][] = new Integer[number_of_vertices][number_of_vertices];
        for (int i=0; i<number_of_vertices; i++) {
            for (int j=0; j<number_of_vertices; j++) {
                matrix[i][j] = default_value;
            }
        }
        return matrix;
    }

    private void mark_direct_paths(List<Edge> edges) {
        edges.forEach(edge -> {
            shortest_paths[edge.get_start_vertex()][edge.get_end_vertex()] = edge.get_wage();
            predecessors[edge.get_start_vertex()][edge.get_end_vertex()] = edge.get_start_vertex();
        });
    }

    private void printMatrix(Integer[][] matrix) {
        for (int i=0 ;i<matrix.length; i++) {
            for (int j=0; j<matrix.length; j++) {
                System.out.print(matrix[i][j].intValue() + " ");
            }
            System.out.println();
        }
        System.out.println("-----------------------------------");
    }
}
