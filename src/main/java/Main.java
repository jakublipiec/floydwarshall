import ancillary.Edge;

import java.util.List;

import static java.util.Arrays.asList;
import static ancillary.Edge.edge;

public class Main {
    public static void main(String... args) {

        final Integer graph_order = 5;
        List<Edge> edges = asList(
                edge(0, 1,5),
                edge(0, 2,4),
                edge(0, 3,8),
                edge(1, 0,-4),
                edge(1, 2,-2),
                edge(1, 4,5),
                edge(2, 3,5),
                edge(2, 4,2),
                edge(3, 0,-1),
                edge(3, 1, 2),
                edge(3, 4,-1),
                edge(4, 2,4),
                edge(4, 3, 2)
        );

        FloydWarshall floydWarshall = FloydWarshall.forGraph(edges, graph_order);

        String result = floydWarshall.calculate()
                ? floydWarshall.getPath(4, 2)
                : "Negative cycle!";

        System.out.println(result);
    }
}
