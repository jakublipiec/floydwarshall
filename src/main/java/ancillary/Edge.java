package ancillary;

public class Edge {
    private int start_vertex;
    private int end_vertex;
    private int wage;

    private Edge(int start_vertex, int end_vertex, int wage) {
        this.start_vertex = start_vertex;
        this.end_vertex = end_vertex;
        this.wage = wage;
    }

    public static Edge edge(int start_vertex, int end_vertex, int wage) {
        return new Edge(start_vertex, end_vertex, wage);
    }

    public int get_start_vertex() {
        return start_vertex;
    }

    public int get_end_vertex() {
        return end_vertex;
    }

    public int get_wage() {
        return wage;
    }
}
